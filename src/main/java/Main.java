import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main {

	public static void main(String[] args) throws IOException, URISyntaxException {

		System.out.println("\n////////////////////////////////////////////////////////////\n////////////////////////////////////////////////////////////\n");

		URL resource = Main.class.getClassLoader().getResource("employee.txt");
		File file = new File(resource.toURI());
		Reader reader = new FileReader(file);

		ObjectMapper objectMapper = new ObjectMapper(); // THE MOST IMPORTANT OBJECT

		Employee emp = objectMapper.readValue(reader, Employee.class);

		System.out.println("Employee name\n" + emp.getName());


		Employee emp1 = createEmployee();

		file = new File("/home/royf/Desktop/example_mine/src/main/resources/employee2.json");
		file.createNewFile();
		Writer writer = new FileWriter(file);

		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		//objectMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
		objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);

		objectMapper.writeValue(writer, emp1);

		System.out.println("\n////////////////////////////////////////////////////////////\n////////////////////////////////////////////////////////////\n");


		////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////


		resource = Main.class.getClassLoader().getResource("data.json");
		file = new File(resource.toURI());

		//converting json to Map
		byte[] mapData = Files.readAllBytes(file.toPath());
		Map<String, String> myMap;

		myMap = objectMapper.readValue(mapData, HashMap.class);
		System.out.println("Map is: " + myMap);


		System.out.println("\n////////////////////////////////////////////////////////////\n////////////////////////////////////////////////////////////\n");

		String jsonString = "{\"stdName\":\"Nicholas\",\"course\":24,\"marks\":[70,90,85]}";
		JsonNode node = objectMapper.readTree(jsonString);
		JsonNode stdName = node.path("stdName");
		System.out.println("Name: " + stdName.textValue());


		System.out.println("\n////////////////////////////////////////////////////////////\n////////////////////////////////////////////////////////////\n");
	}

	public static Employee createEmployee() {

		Employee emp = new Employee();
		emp.setId(100);
		emp.setName("David");
		emp.setPermanent(false);
		emp.setPhoneNumbers(new long[]{123456, 987654});
		emp.setRole("Manager");

		Address add = new Address();
		add.setCity("Bangalore");
		add.setStreet("BTM 1st Stage");
		add.setZipcode(560100);
		emp.setAddress(add);

		List<String> cities = new ArrayList<String>();
		cities.add("Los Angeles");
		cities.add("New York");
		emp.setCities(cities);

		Map<String, String> props = new HashMap<String, String>();
		props.put("salary", "1000 Rs");
		props.put("age", "28 years");
		emp.setProperties(props);

		return emp;
	}

}
